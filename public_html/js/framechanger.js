window.addEventListener("load", function () {
    applyPreferences();
    ajouteClickPositionListener();
    document.querySelector("#frameclosebtn").addEventListener('click', function () {
        document.querySelector("#frameeditor").style.display = "none";
    }, false);
    //readTextFile("request.txt");
    //localStorage.setItem('mycode', text);
    text = localStorage.getItem('mycode');
    textByLine = text.split('\n');
    countNumberPage();
    for (var i = 0; i < nbPage; i++) {
        addList();
    }
    var elements = [];
    for (var i = 0; i < nbPage; i++) {
        elements.push(window.document.querySelector("#button" + i));
    }
    setPagesFromTxt(textByLine);
    elements[0].addEventListener('click', function () {
        elements[0].classList.add("current");
        elements[previous].classList.remove("current");
        previous = 0;
        setIframe(1, generateRandomPage(1));
        setIframe(2, generateRandomPage(2));
        setIframe(3, generateRandomPage(3));
    }, false);
    for (var i = 1; i < nbPage; i++) {
        elements[i].addEventListener('click', function () {
            var cpt = parseInt((this.id).replaceAll("button", "")) + 1;
            elements[cpt - 1].classList.add("current");
            elements[previous].classList.remove("current");
            previous = cpt - 1;
            var currentPage = currentFrame[cpt - 1];
            if (currentFrame[cpt - 1].url === null) {
                setIframe(1, currentPage.frame1);
                setIframe(2, currentPage.frame2);
                setIframe(3, currentPage.frame3);
                currentWebPage = cpt;
            }
            if (currentFrame[cpt - 1].url !== null) {
                var url = currentFrame[cpt - 1].url;
                window.open(url, '_blank');
            }
        }, false);
    }
    setEbayBtnListener(1);
    setEbayBtnListener(2);
    setEbayBtnListener(3);
    setFrameEditorListener(1);
    setFrameEditorListener(2);
    setFrameEditorListener(3);
    elements[0].classList.add("current");
    previous = 0;
    setIframe(1, generateRandomPage(1));
    setIframe(2, generateRandomPage(2));
    setIframe(3, generateRandomPage(3));
    window.onresize = function () {
        setIframePosition(currentFrame1,1);
        setIframePosition(currentFrame2,2);
        setIframePosition(currentFrame3,3);
    };
    QS("#submitform").addEventListener("click",function(){
        // CLICK SUR SUBMIT
        var textSearch = QS("#tags").value;
        var titleOnly = QS("#titre").value;
        var category = QS("#categorie").value;
        var location = QS("#localisation").value;
        if(location === "france"){
            location = null;
        }
        var item_description = QS("#description").value;
        var costMin = QS("#prixMin").value;
        var costMax = QS("#prixMax").value;
        var genFrame = new iframe(textSearch, costMin, costMax, location, titleOnly, category, item_description);
        setIframe(loadedFrame,genFrame);
    });
    QS("#saveObject").addEventListener("click",function(){
        // CLICK SUR SAVE
        var textSearch = QS("#tags").value;
        var titleOnly = QS("#titre").value;
        var category = QS("#categorie").value;
        if(category === '15'){
            category = "INFORMATIQUE";
        }
        var localisation = QS("#localisation").value;
        var item_description = QS("#description").value;
        var costMin = QS("#prixMin").value;
        var costMax = QS("#prixMax").value;
        var genFrame = new iframe(textSearch, costMin, costMax, localisation, titleOnly, category, item_description);
        var code = convertObjectInCode(genFrame);
        replaceFrameCode(currentWebPage,loadedFrame,code);
        localStorage.setItem('mycode', textByLine.join('\n'));
        location.reload();
    });
    titleEditorHandler();
    QS("#submitButton").addEventListener("click", function () {
        localStorage.setItem("pageTitle", QS("#editText").value);
        QS("#editFrame").style.display = "none";
        applyPreferences();
        titleEditorHandler();
    });
}, false);

function convertObjectInCode (frame){
    var code =
    ["FRAME{",
        "TEXT : \"" + frame.textSearch + "\";",
        "PRICE_LOW : " + frame.costMin + ";",
        "PRICE_HIGH : " + frame.costMax + ";",
        "LOCATION : " + frame.location + ";",
        "TITLE_ONLY : " + frame.titleOnly + ";",
        "CATEGORY : " + frame.category + ";",
        "DESCRIPTION : " + frame.item_description + ";",
        "}"
    ];
    return code;
}

function replaceFrameCode (nb1,nb2,code){
    var cpt = 0;
    var lineStartPage;
    for(lineStartPage = 0;cpt < nb1;lineStartPage++){
        if(textByLine[lineStartPage].includes("TITLE :")){
            cpt++;
        }
    }
    var lineStartFrame;
    var cpt2 = 0;
    for(var lineStartFrame = lineStartPage;cpt2 < nb2;lineStartFrame++){
        if(textByLine[lineStartFrame].includes("FRAME{")){
            cpt2++;
        }
    }
    var lineFinishFrame;
    lineStartFrame--;
    for(lineFinishFrame = lineStartFrame;!(textByLine[lineFinishFrame].includes("}"));lineFinishFrame++){}
    lineFinishFrame++;
    //println(textByLine.slice(lineStartFrame,lineFinishFrame+1).join('\n'));
    textByLine.splice(lineStartFrame,lineFinishFrame-lineStartFrame);
    textByLine = textByLine.injectArray(lineStartFrame,code);
    //println(textByLine.join('\n'));
}

function setFrameEditorListener (nb){
    var editor = document.querySelector("#frameeditor");
    switch(nb){
        case 1 :
            document.querySelector("#opt1").addEventListener("click",function(){
                editor.style.display = "unset";
                loadFrameInEditor(currentFrame1);
                loadedFrame = 1;
            });
        break;
        case 2 :
            document.querySelector("#opt2").addEventListener("click",function(){
                editor.style.display = "unset";
                loadFrameInEditor(currentFrame2);
                loadedFrame = 2;
            });
        break;
        case 3 :
            document.querySelector("#opt3").addEventListener("click",function(){
                editor.style.display = "unset";
                loadFrameInEditor(currentFrame3); 
                loadedFrame = 3;
            });
        break;
    }
}

function setEbayBtnListener (nb){
    switch(nb){
        case 1 :
            document.querySelector("#ebaybtn1").addEventListener("click",function(){
                window.open(currentFrame1.getEbayURL,'_blank'); 
            });
        break;
        case 2 :
            document.querySelector("#ebaybtn2").addEventListener("click",function(){
                window.open(currentFrame2.getEbayURL,'_blank'); 
            });
        break;
        case 3 :
            document.querySelector("#ebaybtn3").addEventListener("click",function(){
                window.open(currentFrame3.getEbayURL,'_blank'); 
            });
        break;
    }
}

var mouseX;
var mouseY;
var currentFrame = [];
var shown = false;
var frame1 = document.getElementById('frame1');
var frame2 = document.getElementById('frame2');
var frame3 = document.getElementById('frame3');
var currentWebPage = 1;
var nbPage;
var nbButton = 0;
var text;
var textByLine;
var previous;
var loadedFrame = null;
var currentFrame1;
var currentFrame2;
var currentFrame3;

function ajouteClickPositionListener (){
    if (document.attachEvent) {
        document.attachEvent('onclick', findMouse);
    } else {
        document.addEventListener('click', findMouse);
    }
}

function setIframePosition(frame, nb) {
    // PORTION DE CODE POUR CACHER PARTIES INUTILES SITE (OUTIL DE RECHERCHE + AVERTISSEMENT COOKIES)
    var divScale = window.document.querySelector("#divsize");
    divScale.innerHTML = "Annonces Informatique occasion « " + frame.textSearch.replaceAll("%20"," ") + " » : Toute la France";
    var size;
    if (divScale.clientWidth >= 590) {
        divScale.style.width = "570px";
    }
    var varheight = divScale.clientHeight;
    var size;
    if (frame1.clientWidth >= 970) {
        size = 295 + 25 + varheight + 55 - 28;
    } else {
        size = 60 + 15 + 25 + 50 + varheight;
    }
    
    println("size = "+ size + " inner = "+ window.innerHeight + " varheight " + varheight);
    switch (nb) {
        case 1 :
            frame1.style.top = "" + (258 - size) + 'px';
            frame1.style.height = "" + (window.innerHeight - (258 - size) + 170) + 'px';
            break;
        case 2 :
            frame2.style.top = "" + (258 - size) + 'px';
            frame2.style.height = "" + (window.innerHeight - (258 - size) + 170) + 'px';
            break;
        case 3 :
            frame3.style.top = "" + (258 - size) + 'px';
            frame3.style.height = "" + (window.innerHeight - (258 - size) + 170) + 'px';
            break;
    }
    // -----------------------------------------------------------------------
}

function generateRandomPage(nb) {
    var randomPage = Math.floor(Math.random() * nbPage);
    while (currentFrame[randomPage].frame1 === null) {
        randomPage = Math.floor(Math.random() * nbPage);
    }
    var randomFrame = Math.floor(Math.random() * 3) + 1;
    var chosenFrame;
    switch (randomFrame) {
        case 1:
            chosenFrame = currentFrame[randomPage].frame1;
            break;
        case 2:
            chosenFrame = currentFrame[randomPage].frame2;
            break;
        case 3:
            chosenFrame = currentFrame[randomPage].frame3;
            break;
        default:
            chosenFrame = currentFrame[randomPage].frame1;
            break;
    }
    if((nb ===  2 || nb === 3)&& chosenFrame.textSearch === frame1.textSearch){
        chosenFrame = generateRandomPage(nb);
    }
    if(nb === 3 && chosenFrame.textSearch === frame2.textSearch){
        chosenFrame = generateRandomPage(nb);
    }
    return chosenFrame;
}

function getEbayUrlFromFrame (frame){
    var url = "https://www.ebay.fr/dsc/";
    if(frame.category === "INFORMATIQUE"){
        url+= "Informatique-reseaux/";
    }
    url += "58058/i.html?";
    if(frame.titleOnly === true){
        url+= "?LH_TitleDesc=1";
    }
    if(frame.costLow !== null){
        url+= "&_udlo=" + frame.costMin;
    }
    if(frame.costHigh !== null){
        url+= "&_udhi=" + frame.costMax;
    }
    var tabIS = frame.getArgTabYES();
    var strsearch = "%28";
    for(var i=0;i<tabIS.length;i++){
        if(i>0){
            strsearch+= "%2C";
        }
        strsearch += tabIS[i];
    }
    strsearch += "%29";
    var tabISNOT = frame.getArgTabNO;
    for(var i=0;i<tabISNOT.length;i++){
        strsearch += "+-";
        strsearch += tabISNOT[i];
    }
    url += "&_nkw=" + strsearch.replaceAll(" ","+");
    url+= "&ipg=200";
    /*
     https://www.ebay.fr/dsc/Informatique-reseaux/58058/i.html?LH_TitleDesc=1&_udlo=10&_udhi=100
     &_ipg=200&_ex_kw=pasca+notthis&_in_kw=1&_nkw=text+machin+truc
    
    POUR :
    frame{
        textSearch = text machin truc NOT pasca NOT notthis
        costMin = 10
        costMax = 100
        titleOnly = true;
        location = FRANCE
        category = INFORMATIQUE
    }
    
    compo
    https://www.ebay.fr/dsc/Informatique-reseaux/58058/i.html?LH_TitleDesc=1&_udlo=10&_udhi=100
     &_ipg=200&_ex_kw=pasca+notthis&_in_kw=1&_nkw=text+machin+truc
    
    http://www.helios825.org/url-parameters.php
     */
    return url;
}

function loadFrameInEditor(frame) {
    document.querySelector("#tags").value = frame.textSearch.replaceAll('%20', ' ');
    document.querySelector("#titre").value = frame.titleOnly;
    document.querySelector("#categorie").value = frame.category;
    if (frame.location !== null) {
        document.querySelector("#localisation").value = frame.location;
    } else {
        document.querySelector("#localisation").value = "france";
    }
    document.querySelector("#description").value = frame.item_description;
    document.querySelector("#prixMin").value = frame.costMin;
    document.querySelector("#prixMax").value = frame.costMax;
}

function setIframe(nb, frame) {
    switch (nb) {
        case 1 :
            frame1.src = frame.getURL();
            currentFrame1 = frame;
            break;
        case 2 :
            frame2.src = frame.getURL();
            currentFrame2 = frame;
            break;
        case 3 :
            frame3.src = frame.getURL();
            currentFrame3 = frame;
            break;
    }
    setItemPrice(frame.costMin, frame.costMax, nb);
    setItemDescription(frame.item_description, nb);
    setIframePosition(frame, nb);
}

function setPagesFromTxt(txt) {
    for (var i = 0; i < nbPage; i++) {
        var webPage;
        var lframe1 = null;
        var lframe2 = null;
        var lframe3 = null;
        var icon = null;
        var title = null;
        var url = null;
        var startP = findStartP(i + 1);
        var finishP = findFinishP(i + 1);
        var typeOfPage = txt[startP - 1];
        if (typeOfPage.includes("TITLE")) {
            lframe1 = identifyFrame2(startP, finishP, 1);
            lframe2 = identifyFrame2(startP, finishP, 2);
            lframe3 = identifyFrame2(startP, finishP, 3);
        }
        if (typeOfPage.includes("EXTERNAL_LINK")) {
            url = findURL(startP);
        }
        webPage = new webpage(lframe1, lframe2, lframe3, icon, title, url);
        currentFrame.push(webPage);
    }
}

function titleEditorHandler() {
    window.document.querySelector("#editTitle").addEventListener("click", function () {
        var editor = window.document.querySelector("#editFrame");
        QS("#editText").value = (QS("#retour").innerHTML).replaceAll('<button type="button" id="editTitle">Edit</button>', "");
        editor.style.display = "block";
        println("hey");
        findMouse();
        editor.style.top = mouseX + "px";
        editor.style.left = mouseY + "px";
    });
}

function applyPreferences() {
    var pageTitle;
    if (localStorage.getItem('pageTitle') !== null) {
        pageTitle = localStorage.getItem('pageTitle');
    } else {
        pageTitle = "Antet Depaj v2";
    }
    QS("#retour").innerHTML = pageTitle + '<button type="button" id="editTitle">Edit</button>';
}

function QS(element) {
    return window.document.querySelector(element);
}

function findMouse(e) {
    e = e || window.event;

    var pageX = e.pageX;
    var pageY = e.pageY;

    // IE 8
    if (pageX === undefined) {
        pageX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        pageY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }

    mouseX = pageY;
    mouseY = pageX;
}

function findURL(startP) {
    var i = startP;
    while (!(textByLine[i].includes("URL"))) {
        i++;
    }
    var ret;
    ret = textByLine[i].split('"');
    return ret[1];
}

function setGoodPosition(index) {
    var divScale = window.document.querySelector("#divsize");
    divScale.innerHTML = "Annonces « " + parseText(index) + " » : Toute la France";
    var varheight = divScale.clientHeight;
    var size = 60 + 15 + 25 + 50 + varheight;
    println(">>>>>>" + size);
    return size;
}

// setlocation(x,258 - (60 + 15 + size + 25 +50)

/*
 TODO
 - relative design dynamic scaling
 - local storage data for settings
 - settings {
 Location /perm
 Style /perm
 OVERRIDE LOCATION MODE /temp
 }
 - custom label foreach iframe (NameOfSearch /baliseTXT + price /autoGEN)    [OK]
 - special url label (amazon / ebay / aliEx / etc...)                        [OK]
 - open search in ebay button                                                
 - syncronized scrolling across iframes          [IMPOSSIBLE IN JS]
 - implement custom css in iframes               [IMPOSSIBLE IN JS]
 - url change listener iframes + url passthrough [IMPOSSIBLE IN JS]
 - big iframe for specific ads                   [IMPOSSIBLE IN JS]
 - balise IS and ISNOT for text search
 - integration api lbc ebay ...
 */

function getHTMLfromURL(url) {
    var ret;
    $.getJSON('https://allorigins.me/get?url=' + encodeURIComponent(url) + '&callback=?', function (data) {
        ret = data.contents;
        println(ret);
        var x = document.getElementById("frame1");
        var y = (x.contentWindow || x.contentDocument);
        if (y.document)
            y = y.document;
        y.innerHTML = ret;
        println("finish");
    });
    return ret;
}

function addList() {
    var ul = document.getElementById("list");
    var li = document.createElement("li");
    var a = document.createElement("a");
    var img = document.createElement("img");
    img.src = getImgUrl(nbButton + 1);
    img.classList.add('imgnav');
    if (getTypeOfElement(nbButton + 1) === "EXTERNAL_LINK") {
        a.classList.add('exLink');
    }
    a.setAttribute("id", "button" + nbButton); // added line
    a.appendChild(img);
    li.appendChild(a);
    ul.appendChild(li);
    nbButton++;
}

function getTypeOfElement(index) {
    var cpt = 0;
    for (var i = 0; i < textByLine.length; i++) {
        if (textByLine[i].includes("<")) {
            cpt++;
        }
        if (cpt === index) {
            var type = textByLine[i - 1];
            if (type.includes("TITLE")) {
                return "FRAMEBOX";
            }
            if (type.includes("EXTERNAL_LINK")) {
                return "EXTERNAL_LINK";
            }
        }
    }
    return null;
}

function getImgUrl(index) {
    var cpt = 0;
    for (var i = 0; i < textByLine.length; i++) {
        if (textByLine[i].includes("LOGO")) {
            cpt++;
        }
        if (cpt === index) {
            var imgUrl = textByLine[i].replaceAll("LOGO", "").cleanAll();
            return imgUrl;
        }
    }
    return null;
}

function countNumberPage() {
    nbPage = 0;
    for (var i = 0; i < textByLine.length; i++) {
        if (textByLine[i].includes("<")) {
            nbPage++;
        }
    }
}

function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if (rawFile.readyState === 4)
        {
            if (rawFile.status === 200 || rawFile.status === 0)
                ;
            {
                var allText = rawFile.responseText;
                //alert(allText);
                text = allText;
            }
        }
    };
    rawFile.send(null);
}

function findStartPoint(line) {
    for (var i = 0; i < line.length; i++) {
        if (line.charAt(i) === ':') {
            return i + 1;
        }
    }
}

function parsePriceLow(line) {
    return parseInt(line.replaceAll("PRICE_LOW", "").cleanAll());
}

function parsePriceHigh(line) {
    return parseInt(line.replaceAll("PRICE_HIGH", "").cleanAll());
}

function parseCategory(line) {
    var category = line.replaceAll("CATEGORY", "").cleanAll();
    switch (category.toUpperCase()) {
        case "INFORMATIQUE" :
            return 15;
            break;
        default :
            return null;
            break;
    }
}

function parseItemDescription(line) {
    var desc = line.replaceAll("DESCRIPTION", "").cleanAllNotSpace(false);
    var i;
    for (i = 0; desc.charAt(i) === ' '; i++) {
    }
    desc = desc.substr(i, desc.length - i);
    return desc;
}

function parseLocation(line) {
    var location = line.replaceAll("LOCATION", "").cleanAll();
    var rt = null;
    switch (location.toUpperCase()) {
        case "FRANCE" :
            rt = null;
            break;
        case "MENTON" :
            rt = "lat=43.7838158&lng=7.5081248&radius=30000";
            break;
        case "STRAPHAEL" :
            rt = "lat=43.424088&lng=6.769596&radius=30000";
            break;
        case "TOULON" :
            rt = "lat=43.124403&lng=5.927216&radius=30000";
            break;
    }
    return rt;
}

function parseTitleOnly(line) {
    var titleOnly = (line.replaceAll("TITLE_ONLY", "")).cleanAll();
    switch (titleOnly.toUpperCase()) {
        case "TRUE" :
            return true;
            break;
        case "FALSE" :
            return false;
            break;
        case "Y" :
            return true;
            break;
        case "N" :
            return false;
            break;
        default :
            return null;
            break;
    }
}

function findLineStartF(startP, finishP, nb) {
    var cpt = 0;
    for (var i = startP; i < finishP; i++) {
        if (textByLine[i].includes("{")) {
            cpt++;
        }
        if (cpt === nb) {
            return i;
        }
    }
}

function findLineFinishF(startP, finishP, nb) {
    var cpt = 0;
    for (var i = startP; i < finishP; i++) {
        if (textByLine[i].includes("}")) {
            cpt++;
        }
        if (cpt === nb) {
            return i;
        }
    }
}

function setItemDescription(string, id) {
    itmdesc = window.document.querySelector("#desc" + id);
    itmdesc.innerHTML = string;
}

function setItemPrice(low, high, id) {
    itmdesc = window.document.querySelector("#cost" + id);
    if (low === null || low === "nan") {
        low = '?';
    }
    if (high === null || high === "nan") {
        high = '?';
    }
    itmdesc.innerHTML = "[" + low + "-" + high + "]€";
}

class iframe {
    constructor(textSearch, costMin, costMax, location, titleOnly, category, item_description) {
        this.textSearch = textSearch;
        this.costMin = costMin;
        this.costMax = costMax;
        this.location = location;
        this.titleOnly = titleOnly;
        this.category = category;
        this.item_description = item_description;
    }
    getURL() {
        var link = "https://www.leboncoin.fr/recherche/?" + "text=" + this.textSearch;
        if (this.category !== null) {
            link += "&category=" + this.category;
        }
        if (this.location !== null) {
            link += "&" + this.location;
        }
        if (this.titleOnly !== null && this.titleOnly !== false) {
            link += "&search_in=subject";
        }
        if (this.costMin !== null || this.costMax !== null) {
            link += "&price=";
            if (this.costMin !== null) {
                link += "" + this.costMin + "-";
            } else {
                link += "nan-";
            }
            if (this.costMax !== null) {
                link += "" + this.costMax;
            } else {
                link += "nan";
            }
        }
        return link;
    }
    print(){
        println("------ IFRAME -------");
        println("textSearch : " + this.textSearch);
        println("costMin : " + this.costMin);
        println("costMax : " + this.costMax);
        println("location : " + this.location);
        println("titleOnly : " + this.titleOnly);
        println("category : " + this.category);
        println("item_description : " + this.item_description);
        println("---------------------");
    }
    getTextSearch(){
        return this.textSearch.replaceAll("%20"," ");
    }
    getArgTabYES(){
        var txtTAB = this.getArgTab();
        println(txtTAB);
        for(var i=0;i<txtTAB.length;i++){
            if(txtTAB[i].includes("<N>")){
                txtTAB.splice(i,1);
                i--;
            }
            else{
                txtTAB[i] = removeUselessSpace(txtTAB[i].replaceAll("<Y>",""));
            }
        }
        return txtTAB;
    }
    getArgTabNO(){
        var txtTAB = this.getArgTab();
        println(txtTAB);
        for(var i=0;i<txtTAB.length;i++){
            if(txtTAB[i].includes("<Y>")){
                txtTAB.splice(i,1);
                i--;
            }
            else{
                txtTAB[i] = removeUselessSpace(txtTAB[i].replaceAll("<N>",""));
            }
        }
        return txtTAB;
    }
    getArgTab(){
        var txt = "<Y> " + this.getTextSearch();
        println(this.getTextSearch());
        txt = txt.replaceAll(" OR ","|<Y>");
        txt = txt.replaceAll(" NOT ","|<N>");
        var txtTAB = txt.split("|");
        return txtTAB;
    }
    getEbayURL (){
        var url = "https://www.ebay.fr/dsc/";
        if(this.category === "INFORMATIQUE"){
            url+= "Informatique-reseaux/";
        }
        url += "58058/i.html?";
        if(this.titleOnly === true){
            url+= "?LH_TitleDesc=1";
        }
        if(this.costLow !== null){
            url+= "&_udlo=" + this.costMin;
        }
        if(this.costHigh !== null){
            url+= "&_udhi=" + this.costMax;
        }
        var tabIS = this.getArgTabYES();
        var strsearch = "%28";
        for(var i=0;i<tabIS.length;i++){
            if(i>0){
                strsearch+= "%2C";
            }
            strsearch += tabIS[i];
        }
        strsearch += "%29";
        var tabISNOT = this.getArgTabNO;
        for(var i=0;i<tabISNOT.length;i++){
            strsearch += "+-";
            strsearch += tabISNOT[i];
        }
        url += "&_nkw=" + strsearch.replaceAll(" ","+");
        url+= "&ipg=200";
        return url;
    }
}

class webpage {
    constructor(frame1, frame2, frame3, icon, title, url) {
        this.frame1 = frame1;
        this.frame2 = frame2;
        this.frame3 = frame3;
        this.icon = icon;
        this.title = title;
        this.url = url;
    }
}

function findEnd(text, charOpen, charClose) {
    var cpt1 = 0;
    var cpt2 = 0;
    var textMod = [];
    for (var i = 0; i < text.length; i++) {
        var lineMod = "";
        for (var i2 = 0; i2 < text[i].length; i2++) {
            var charNow = text[i].charAt(i2);
            if (charNow === charOpen) {
                cpt1++;
            }
            if (charNow === charClose) {
                cpt2++;
            }
            if (cpt1 === cpt2) {
                i2 = 9999999;
                i = 9999999;
            }
            if (cpt1 > 0) {
                lineMod += charNow;
            }
        }
        textMod.push(lineMod);
    }
    return textMod;
}

function identifyFrame2(startP, finishP, nb) {
    var lineStart = findLineStartF(startP, finishP, nb);
    var lineFinish = findLineFinishF(startP, finishP, nb);
    var textSearch = null;
    var costMin = null;
    var costMax = null;
    var location = null;
    var titleOnly = null;
    var category = null;
    for (var i = lineStart; i < lineFinish; i++) {
        var line = textByLine[i];
        if (line.includes("TEXT")) {
            textSearch = remove2space(parseText(i)).replaceAll("	","");
        }
        if (textByLine[i].includes("PRICE_LOW")) {
            costMin = parsePriceLow(line);
        }
        if (textByLine[i].includes("PRICE_HIGH")) {
            costMax = parsePriceHigh(line);
        }
        if (textByLine[i].includes("LOCATION")) {
            location = parseLocation(line);
        }
        if (textByLine[i].includes("TITLE_ONLY")) {
            titleOnly = parseTitleOnly(line);
        }
        if (textByLine[i].includes("CATEGORY")) {
            category = parseCategory(line);
        }
        if (textByLine[i].includes("DESCRIPTION")) {
            var item_description;
            item_description = parseItemDescription(line);
        }
    }
    var frame = new iframe(textSearch, costMin, costMax, location, titleOnly, category, item_description);
    return frame;
}

function identifyFrame(startP, finishP, nb) {
    var lineStart = findLineStartF(startP, finishP, nb);
    var lineFinish = findLineFinishF(startP, finishP, nb);
    var textSearch = null;
    var costMin = null;
    var costMax = null;
    var location = null;
    var titleOnly = null;
    var category = null;
    for (var i = lineStart; i < lineFinish; i++) {
        var line = textByLine[i];
        if (line.includes("TEXT")) {
            textSearch = remove2space(parseText(i));

            // PORTION DE CODE POUR CACHER PARTIES INUTILES SITE (OUTIL DE RECHERCHE + AVERTISSEMENT COOKIES)
            var divScale = window.document.querySelector("#divsize");
            divScale.innerHTML = "Annonces « " + parseText(i) + " » : Toute la France";
            var size;
            if (divScale.clientWidth >= 590) {
                divScale.style.width = "590px";
            }
            var varheight = divScale.clientHeight;
            var size;
            if (frame1.clientWidth >= 970) {
                size = 295 + 25 + varheight + 55;
            } else {
                size = 60 + 15 + 25 + 50 + varheight;
            }

            switch (nb) {
                case 1 :
                    frame1.style.top = "" + (258 - size) + 'px';
                    frame1.style.height = "" + (window.innerHeight - (258 - size) + 170) + 'px';
                    break;
                case 2 :
                    frame2.style.top = "" + (258 - size) + 'px';
                    frame2.style.height = "" + (window.innerHeight - (258 - size) + 170) + 'px';
                    break;
                case 3 :
                    frame3.style.top = "" + (258 - size) + 'px';
                    frame3.style.height = "" + (window.innerHeight - (258 - size) + 170) + 'px';
                    break;
            }
            // -----------------------------------------------------------------------

        }
        if (textByLine[i].includes("PRICE_LOW")) {
            costMin = parsePriceLow(line);
        }
        if (textByLine[i].includes("PRICE_HIGH")) {
            costMax = parsePriceHigh(line);
        }
        if (textByLine[i].includes("LOCATION")) {
            location = parseLocation(line);
        }
        if (textByLine[i].includes("TITLE_ONLY")) {
            titleOnly = parseTitleOnly(line);
        }
        if (textByLine[i].includes("CATEGORY")) {
            category = parseCategory(line);
        }
        if (textByLine[i].includes("DESCRIPTION")) {
            var item_description;
            item_description = parseItemDescription(line);
            setItemDescription(item_description, nb);
        }
    }
    setItemPrice(costMin, costMax, nb);
    var link = "https://www.leboncoin.fr/recherche/?" + "text=" + textSearch;
    if (category !== null) {
        link += "&category=" + category;
    }
    if (location !== null) {
        link += "&" + location;
    }
    if (titleOnly !== null && titleOnly !== false) {
        link += "&search_in=subject";
    }
    if (costMin !== null || costMax !== null) {
        link += "&price=";
        if (costMin !== null) {
            link += "" + costMin + "-";
        } else {
            link += "nan-";
        }
        if (costMax !== null) {
            link += "" + costMax;
        } else {
            link += "nan";
        }
    }
    println(link);
    var frame = new iframe(textSearch, costMin, costMax, location, titleOnly, category, item_description);
    return link;
}

function findStartP(pt) {
    var cp = 0;
    for (var i = 0; i < textByLine.length; i++) {
        if (textByLine[i].includes("<")) {
            cp++;
            if (cp === pt) {
                return i;
            }
        }
    }
    return null;
}

function findFinishP(pt) {
    var cp = 0;
    for (var i = 0; i < textByLine.length; i++) {
        if (textByLine[i].includes(">")) {
            cp++;
            if (cp === pt) {
                return i;
            }
        }
    }
    return null;
}

function makeLink(i) {
    frameLink1 = "https://www.leboncoin.fr/recherche/?";
    var textSearch = "text=";
    textSearch += parseText(i);
    textSearch = remove2space(textSearch);
    frameLink1 += textSearch + "&category=15";
    console.log(frameLink1);
    return frameLink1;
}

function parseText(nbFirstLine) {
    var ret = (textByLine[nbFirstLine]).replaceAll("TEXT","").replaceAll(":","");
    for(var i=nbFirstLine+1;!(textByLine[i].includes(":"));i++){
        ret += " " + textByLine[i];
    }
    ret = ret.replaceAll("\"", "").replaceAll("\r", "").replaceAll(";", "").replaceAll("\n", "").replaceAll("	","");
    return ret;
}

function remove2space(string) {
    return string.replaceAll(" ", "%20");
}

function println(string) {
    console.log(string);
}

function removeUselessSpace (str){
    var ret;
    var start;
    for(start =0;str.charAt(start)===' ';start++){}
    var finish;
    for(finish = str.length-1;str.charAt(finish)===' ';finish--){}
    ret = str.substr(start,finish-start+1);
    return ret;
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

String.prototype.cleanAll = function () {
    var target = this;
    target = target.replaceAll(":", "");
    target = target.replaceAll(" ", "");
    target = target.replaceAll("	", "");
    target = target.replaceAll("\r", "");
    target = target.replaceAll(";", "");
    target = target.replaceAll("\n", "");
    return target;
};

String.prototype.cleanAllNotSpace = function () {
    var target = this;
    target = target.replaceAll(":", "");
    target = target.replaceAll("	", "");
    target = target.replaceAll("\r", "");
    target = target.replaceAll(";", "");
    target = target.replaceAll("\n", "");
    return target;
};

Array.prototype.injectArray = function( idx, arr ) {
    return this.slice( 0, idx ).concat( arr ).concat( this.slice( idx ) );
};